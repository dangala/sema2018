Ejercicio 6.1 - Alarma básica

En el subdirectorio ./model se encuentran los ficheros que describen el modelado del sistema.

El sistema se ha modelado como una máquina de estados finitos (FSM) con interrupciones jerárquica, donde el paso del estado inactivo al activo está condicionado a la pulsación de uno de los botones. Esto puede verse en la imagen "interrupt_fsm.png".

En el estado activo una FSM atiende a la interrupción, llevando a cabo el sistema antirrebotes y transmitiendo a la FSM que controla las luces el evento de la pulsación a través de una variable compartida. Esto puede verse en la imagen "debounce.png".

En el estado inactivo una FSM enciende las luces en función del valor de dicha variable compartida y las apaga cuando han transcurrido 60 segundos desde su encendido. Esto puede verse en la imagen "lights.png". Concurrentemente, otra FSM gestiona la alarma, comprobando las señales de armamento y presencia para ello. Cuando la alarma está armada y se detecta presencia, se pasa al estado INTRUSO, en el que se genera una señal PWM que en la implementación excita un buzzer conectado a la plataforma de desarrollo. Cabe destacar que, una vez alcanzado el estado INTRUSO, se puede volver al estado DESARMADA si se desactiva la señal de armar. Esto puede verse en la imagen "alarm.png".

El sistema completo ha sido implementado sobre FreeRTOS, desarrollado y probado con las herramientas de compilación cruzada "arm-none-eabi-gcc" y "arm-none-eabi-gdb", así como con un fichero Makefile cuya autoría corresponde al profesor de la ETSIT, Álvaro Gutiérrez Martín. Finalmente, se ha probado la implementación en la plataforma de desarrollo STM32NUCLEO-L476RG.
