Ejercicio 8 - Planificación YDS

Para el desarrollo de este ejercicio, se ha empleado el código del Ejercicio 7. 

En primer lugar, para poder realizar una planificación basada en el algoritmo YDS es necesario conocer los tiempos de cómputo de las tareas, sus tiempos de activación y sus plazos. Dado que el problema no es restrictivo al respecto, se han establecido plazos iguales a los periodos y periodos iguales entre las tareas, siendo por tanto el plazo para todas las tareas de 200ms. Además, puesto que buscamos analizar el comportamiento en caso peor, supondremos que en el instante inicial todas las tareas están activadas.

A continuación hay que medir el tiempo máximo de cómputo de cada una de las tareas, para lo cual se ha recurrido a la función xTaskGetTickCount() de FreeRTOS, midiendo antes y después de cada llamada a fsm_fire() y promediando las medidas por el número de pruebas. Lamentablemente, FreeRTOS nos ofrece una resolución máxima de 1ms, por lo que al ser los tiempos de cómputo en caso peor menores a este valor, los promedios medidos son siempre cero. Considerando esto, haremos una planificación conservadora en la que suponemos que todas las tareas tienen un tiempo de cómputo en caso peor de exactamente 1ms.

Así pues, activándose todas las tareas en el instante inicial y al tener el mismo periodo y plazo, existe un único intervalo de actividad, donde la intensidad del intervalo viene dada por:

  G([0,200]) = techo( (C_lights + C_alarm + C_decoder) / T ) = techo( ( 1 + 1 + 1 ) / 200 ) = 1

Por lo que el ejecutivo cíclico construido consistirá en el disparo secuencial de las tres FSMs a una frecuencia igual a G([0,200]) veces la frecuencia nominal, es decir, a frecuencia nominal. En el código se ha implementado la función os_update_cpu_frequency() para simular este "cambio" de frecuencias en la ejecución del programa.

