Ejercicio 7 - Alarma con código

En el subdirectorio ./model se encuentran los ficheros que describen el modelado del sistema.

El sistema se ha modelado como una máquina de estados finitos (FSM) con interrupciones jerárquica, donde el paso del estado inactivo al activo está condicionado a la pulsación de uno de los botones. Esto puede verse en la imagen "interrupt_fsm.png".

En el estado activo una FSM extendida atiende a la pulsación de los botones, llevando a cabo el sistema antirrebotes y transmitiendo a las FSMs que controlan las luces y la entrada del código el evento de la pulsación a través de variables compartidas. Esto puede verse en la imagen "debounce.png".

En el estado inactivo, una FSM extendida enciende las luces en función del valor de la variable compartida "button" y las apaga cuando han transcurrido 60 segundos desde su encendido. Esto puede verse en la imagen "lights.png".
Concurrentemente, otra FSM extendida gestiona la alarma comprobando la señal de presencia y la variable compartida "code_entered" para ello. Cuando la alarma está armada y se detecta presencia, se pasa al estado INTRUSO, en el que se genera una señal PWM que en la implementación excita un buzzer conectado a la plataforma de desarrollo. Si se repite la entrada correcta del código se puede volver al estado DESARMADA, pasar de DESARMADA a ARMADA y viceversa. Esto puede verse en la imagen "alarm.png".
Por último, una tercera FSM extendida se encarga de gestionar la entrada del código en función de la variable compartida "cbutton". Cuando el código introducido es correcto, lo señaliza a través de la variable compartida "code_entered". Cabe destacar que en este modelo, es necesario introducir los 3 dígitos para volver al estado de espera, por lo que un error en el primer dígito obliga a introducir dos códigos para lograr el cambio en la variable "code_entered". Esto puede verse en la imagen "decoder.png".

El sistema completo ha sido implementado sobre FreeRTOS, desarrollado y probado con las herramientas de compilación cruzada "arm-none-eabi-gcc" y "arm-none-eabi-gdb", así como con un fichero Makefile cuya autoría corresponde al profesor de la ETSIT, Álvaro Gutiérrez Martín. Finalmente, se ha probado la implementación en la plataforma de desarrollo STM32NUCLEO-L476RG.
